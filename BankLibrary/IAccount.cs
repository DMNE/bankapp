﻿namespace BankLibrary
{
    public interface IAccount
    {
        void PutMoney(decimal sum);

        decimal WithdrawMoney(decimal sum);
    }
}
