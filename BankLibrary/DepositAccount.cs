﻿namespace BankLibrary
{
    public class DepositAccount : Account
    {
        public DepositAccount(decimal sum, int percentage) : base(sum, percentage) { }

        protected internal override void Open()
        {
            base.OnOpened(new AccountEventArgs($"Открыт новый депозитный счет! Id счета: {this.Id}", this.Sum));
        }

        public override void PutMoney(decimal sum)
        {
            if (_days % 30 == 0)
                base.PutMoney(sum);
            else
                base.OnAdded(new AccountEventArgs("На счет можно положить только после 30-дневного периода", 0));
        }

        public override decimal WithdrawMoney(decimal sum)
        {
            if (_days % 30 == 0)
                return base.WithdrawMoney(sum);
            else
                base.OnWithdrawed(new AccountEventArgs("Вывести средства можно только после 30-дневного периода", 0));
            return 0;
        }

        protected internal override void Calculate()
        {
            if(_days % 30 == 0)
                base.Calculate();
        }
    }
}
